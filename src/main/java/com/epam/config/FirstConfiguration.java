package com.epam.config;

import com.epam.config.profile.ProfileConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({SecondConfiguration.class, ProfileConfig.class})
@ComponentScans({
        @ComponentScan("com.epam.model.bean1"),
        @ComponentScan("com.epam.model.otherbean")
})
public class FirstConfiguration {
}
