package com.epam.config.profile;

import com.epam.model.bean2.CatAnimal;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("role.properties")
public class ProfileConfig {

    @Bean
    @Profile("admin")
    public CatAnimal getAdminCat(@Value("${admin.name}") String name,
                        @Value("${admin.yearsOld}") int yearsOld) {
        return new CatAnimal(name, yearsOld);
    }

    @Bean
    @Profile("user")
    public CatAnimal getUserCat(@Value("${user.name}") String name,
                            @Value("${user.yearsOld}") int yearsOld) {
        return new CatAnimal(name, yearsOld);
    }
}
