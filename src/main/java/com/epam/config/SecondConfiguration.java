package com.epam.config;

import com.epam.model.bean2.CatAnimal;
import com.epam.model.bean3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans({
        @ComponentScan(value = "com.epam.model.bean2",
        excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE,classes = CatAnimal.class)),
        @ComponentScan(value = "com.epam.model.bean3",
        excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE,classes = BeanE.class))})
public class SecondConfiguration {
}
