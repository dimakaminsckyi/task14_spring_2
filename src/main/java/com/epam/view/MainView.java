package com.epam.view;

import com.epam.controller.TaskController;
import com.epam.controller.TaskControllerImpl;
import com.epam.view.util.PropertiesRead;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private PropertiesRead read = new PropertiesRead("menu.properties");
    private Scanner scanner;
    private TaskController controller;
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public MainView() {
        scanner = new Scanner(System.in);
        initMenu();
        initController(scanner);
        methodRun = new LinkedHashMap<>();
        methodRun.put("show", () -> controller.showAllBean());
        methodRun.put("list", () -> controller.printListBean());
        methodRun.put("scope", () -> controller.printBeanWithScope());
        methodRun.put("profile", () -> controller.printBeanWithProfile());
        methodRun.put("exit", () -> controller.exit());
    }

    public void runMenu() {
        StringBuilder flagExit = new StringBuilder("");
        do {
            menu.values().forEach(log::info);
            try {
                flagExit.setLength(0);
                flagExit.append(scanner.nextLine());
                methodRun.get(flagExit.toString()).run();
            } catch (NullPointerException e) {
                log.error("Input correct option");
            }
        } while (!flagExit.toString().equals("exit"));
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("show", read.getValue("show"));
        menu.put("list", read.getValue("list"));
        menu.put("scope", read.getValue("scope"));
        menu.put("profile", read.getValue("profile"));
        menu.put("exit", read.getValue("exit"));
    }

    private void initController(Scanner scanner){
        log.info("Enter profile for context(admin / user) : ");
        controller = new TaskControllerImpl(scanner.nextLine());
    }
}


