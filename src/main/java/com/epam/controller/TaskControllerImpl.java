package com.epam.controller;

import com.epam.config.FirstConfiguration;
import com.epam.model.bean2.CatAnimal;
import com.epam.model.otherbean.ListBean;
import com.epam.model.otherbean.OtherBeanA;
import com.epam.model.otherbean.OtherBeanB;
import com.epam.model.otherbean.OtherBeanC;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TaskControllerImpl implements TaskController {

    private Logger log = LogManager.getLogger(TaskControllerImpl.class);
    private AnnotationConfigApplicationContext context;

    public TaskControllerImpl(String profile) {
        context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles(profile);
        context.register(FirstConfiguration.class);
        context.refresh();
    }

    @Override
    public void showAllBean() {
        for (String beanName : context.getBeanDefinitionNames()){
            log.info(beanName);
        }
    }

    @Override
    public void printListBean() {
        ListBean listBean = context.getBean(ListBean.class);
        listBean.printBeans();
    }

    @Override
    public void printBeanWithScope() {
        log.info("Singleton");
        log.info(context.getBean(OtherBeanA.class));
        log.info(context.getBean(OtherBeanA.class));
        log.info("Prototype");
        log.info(context.getBean(OtherBeanB.class));
        log.info(context.getBean(OtherBeanB.class));
        log.info(context.getBean(OtherBeanC.class));
        log.info(context.getBean(OtherBeanC.class));
    }

    @Override
    public void printBeanWithProfile() {
        log.info(context.getBean(CatAnimal.class).toString());
    }

    @Override
    public void exit() {
        log.info("Closing context ... ");
        context.close();
        log.info("Exit");
    }
}
