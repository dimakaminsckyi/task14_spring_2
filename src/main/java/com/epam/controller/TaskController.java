package com.epam.controller;

public interface TaskController {

    void showAllBean();

    void printListBean();

    void printBeanWithScope();

    void printBeanWithProfile();

    void exit();
}
