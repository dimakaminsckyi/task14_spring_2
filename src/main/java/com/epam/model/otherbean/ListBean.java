package com.epam.model.otherbean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ListBean {

    private static Logger log = LogManager.getLogger(ListBean.class);
    @Autowired
    private List<BaseBean> baseBeanList;

    public void printBeans(){
        for (BaseBean baseBean : baseBeanList){
            log.info(baseBean.getBeanName());
        }
    }
}
