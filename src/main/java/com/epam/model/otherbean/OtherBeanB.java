package com.epam.model.otherbean;

import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Order(2)
public class OtherBeanB implements BaseBean {

    @Override
    public String getBeanName() {
        return getClass().getSimpleName();
    }
}
