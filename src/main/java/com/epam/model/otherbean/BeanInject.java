package com.epam.model.otherbean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanInject {

    @Autowired
    @Qualifier("otherBeanB")
    private BaseBean baseBeanB;
    private BaseBean baseBeanA;
    private BaseBean baseBeanC;

    @Autowired
    public BeanInject(OtherBeanA otherBeanA) {
        this.baseBeanA = otherBeanA;
    }

    public BaseBean getBaseBeanB() {
        return baseBeanB;
    }

    public BaseBean getBaseBeanA() {
        return baseBeanA;
    }

    public BaseBean getBaseBeanC() {
        return baseBeanC;
    }

    @Autowired
    @Qualifier("otherBeanC")
    public void setBaseBeanC(BaseBean baseBeanC) {
        this.baseBeanC = baseBeanC;
    }
}
