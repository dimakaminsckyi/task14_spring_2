package com.epam.model.otherbean;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Primary
@Scope("singleton")
@Order(1)
public class OtherBeanA implements BaseBean {

    @Override
    public String getBeanName() {
        return getClass().getSimpleName();
    }
}
