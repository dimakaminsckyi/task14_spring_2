package com.epam.model.bean2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {

    private String name;
    private int yearsOld;

    public CatAnimal() {
    }

    public CatAnimal(String name, int yearsOld) {
        this.name = name;
        this.yearsOld = yearsOld;
    }

    public String getName() {
        return name;
    }

    public int getYearsOld() {
        return yearsOld;
    }

    @Override
    public String toString() {
        return "CatAnimal{" +
                "name='" + name + '\'' +
                ", yearsOld=" + yearsOld +
                '}';
    }
}
