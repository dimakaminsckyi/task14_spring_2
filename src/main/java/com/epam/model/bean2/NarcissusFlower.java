package com.epam.model.bean2;

import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {

    private String name;
    private int yearsOld;

    public NarcissusFlower() {
    }

    public String getName() {
        return name;
    }

    public int getYearsOld() {
        return yearsOld;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "name='" + name + '\'' +
                ", yearsOld=" + yearsOld +
                '}';
    }
}
