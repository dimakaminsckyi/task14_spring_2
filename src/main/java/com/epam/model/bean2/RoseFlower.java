package com.epam.model.bean2;

import org.springframework.stereotype.Component;

@Component
public class RoseFlower {

    private String name;
    private int yearsOld;

    public RoseFlower() {
    }

    public String getName() {
        return name;
    }

    public int getYearsOld() {
        return yearsOld;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "name='" + name + '\'' +
                ", yearsOld=" + yearsOld +
                '}';
    }
}
