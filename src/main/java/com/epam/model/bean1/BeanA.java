package com.epam.model.bean1;

import org.springframework.stereotype.Component;

@Component
public class BeanA {

    private String name;
    private int value;

    public BeanA() {
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
