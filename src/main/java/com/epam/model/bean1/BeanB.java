package com.epam.model.bean1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {


    private String name;
    private int value;

    public BeanB() {
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
