package com.epam.model.bean1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {

    private String name;
    private int value;

    public BeanC() {
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
