package com.epam.model.bean3;

import org.springframework.stereotype.Component;

@Component
public class BeanD {

    private String name;
    private int value;

    public BeanD() {
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
